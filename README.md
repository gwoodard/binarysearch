# Binary Search Algorithm #

### **About:** ###

This repo contains source for a Binary Search algorithm assignment. The user will first enter the number of integers then enter the search key. The program will either determine the position of the integer in the array or display the integer is not found in the array.

### **IDE:**
Visual Studio

### **Input:** ###
Numbers in the array, Search Key

### **Language:** ###
C++