#include <iostream>
using namespace std;

int BinarySearch(int A[], int p, int r, int V)
{
	if (r < p)
	{
		int mid = (r + p) / 2;
		if (V == A[mid]) return mid;
		else
		{
			if (V < A[mid]) return BinarySearch(A, p, mid - 1, V);
			else return BinarySearch(A, mid + 1, r, V);
		}
	}
	else
	{
		if (V == A[p]) return p;
		return -1;
	}
}

int main()
{
	int *A;
	int N; //N is no data
	cout << "Enter # of data: ";
	cin >> N;
	A = new int[N]; //dynamic array
	cout << "Enter " << N << " numbers of sorted data";
	int i, v, j;
	for (i = 0; i <= N - 1; i++)
		cin >> A[i];
	cout << "Enter search key ";
	cin >> v;
	j = BinarySearch(A, 0, N - 1, v);
	if (j == -1) cout << v << " is not found in the array" << endl;
	else cout << v << " is in " << j << "th position in the array" << endl;
	system("pause");
	return 0;

}